#!/usr/bin/env zsh

function sexy-source() {
    local cfg
    cfg="${SEXY_ROOT}/${1}/cfg/${2}"

    if [ ! -e "${cfg}" ]; then
        echo " * SEXY * >>> No ${1}/${2} config file found. Creating from sample [${cfg}.dist -> ${cfg}]"
        cp "${cfg}.dist" "${cfg}"
    fi

    source "${cfg}"
}

function sexy-dot-source() {
    if [ -e "${ORIGZDOTDIR}/${1}" ]; then
        source "${ORIGZDOTDIR}/${1}"
    fi
}

function sexy-pathmunge()
{
    setopt localoptions equals

    if ! echo $PATH | =grep -qE "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
}
